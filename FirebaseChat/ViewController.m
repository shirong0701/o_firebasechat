//
//  ViewController.m
//  FirebaseChat
//
//  Created by Billy on 23/03/15.
//  Copyright (c) 2015 Billy. All rights reserved.
//

#import "ViewController.h"
#import "Chat/ChatView.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize users;
@synthesize firebase;
@synthesize chat;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self setTitle:@"Users"];
//    [[self navigationController] setNavigationBarHidden:YES];
    
    NSDictionary * user1 = [[NSDictionary alloc] initWithObjectsAndKeys:@"girl_friend", @"name", @"room1", @"room", @"girl_friend", @"imgName", nil];
    NSDictionary * user2 = [[NSDictionary alloc] initWithObjectsAndKeys:@"billy", @"name", @"room1", @"room", @"billy", @"imgName", nil];
    
    users = [[NSMutableArray alloc] init];
    
    [users addObject:user1];
    [users addObject:user2];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.users count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MessagesCell";
    MessagesCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSDictionary * user = [self.users objectAtIndex:indexPath.row];
    
    [cell setdData:user];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary * user = [self.users objectAtIndex:indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
//    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Message" message:[NSString stringWithFormat:@"name : %@ , room : %@", user[@"name"], user[@"room"]] delegate:nil cancelButtonTitle:@"YES" otherButtonTitles:nil, nil];
    
//    [alert show];
    
    ChatView *chatView = [[ChatView alloc] initWith:user[@"room"] senderId:user[@"name"] displayName:user[@"name"]];
    chatView.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatView animated:YES];
    
}

@end
