//
//  ViewController.h
//  FirebaseChat
//
//  Created by Billy on 23/03/15.
//  Copyright (c) 2015 Billy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Firebase/Firebase.h>
#import "MessageCell/MessagesCell.h"

@interface ViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>


@property (nonatomic, strong) NSMutableArray * users;
@property (nonatomic, strong) NSMutableArray * chat;
@property (nonatomic, strong) Firebase * firebase;
@end

