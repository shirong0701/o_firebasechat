
//
//  MessageCell.m
//  FirebaseChat
//
//  Created by Billy on 23/03/15.
//  Copyright (c) 2015 Billy. All rights reserved.
//

#import "MessagesCell.h"

@interface MessagesCell()
{
	
}
@end

@implementation MessagesCell

@synthesize imgUser, lblUserName, user;

- (void)setdData:(NSDictionary *)user_
{
	user = user_;

    self.imgUser.layer.masksToBounds = YES;
	self.imgUser.layer.cornerRadius = 25;

    NSString * imgName = user[@"imgName"];
    NSString * userName = user[@"name"];
    
    [imgUser setImage:[UIImage imageNamed:imgName]];
    
    [lblUserName setText:userName];
}

@end
