
//
//  MessageCell.h
//  FirebaseChat
//
//  Created by Billy on 23/03/15.
//  Copyright (c) 2015 Billy. All rights reserved.
//


#import <Parse/Parse.h>

@interface MessagesCell : UITableViewCell


@property (nonatomic, strong) NSDictionary * user;
@property (weak, nonatomic) IBOutlet UIImageView *imgUser;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;

- (void)setdData:(NSDictionary *)user_;

@end
