//
//  ChatView.h
//  FirebaseChat
//
//  Created by Billy on 23/03/15.
//  Copyright (c) 2015 Billy. All rights reserved.
//
#import <Firebase/Firebase.h>

#import "JSQMessages.h"

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface ChatView : JSQMessagesViewController
//-------------------------------------------------------------------------------------------------------------------------------------------------

- (id)initWith:(NSString *)groupId_ senderId:(NSString *)senderId_ displayName:(NSString *)displayName_;

@property (strong, nonatomic) Firebase *firebase;

@end
