#  Chat Example  #

iOS Chat Sample App implemented in Obj-C.

# Features
- You can send and reveive messages.
- Service : Firebase, Parse

## Configure Firebase
- Create a new [Firebase](https://www.firebase.com/).
- Open `FirebaseChat.xcodeproj` in Xcode.
- Edit [`AppConstant.h`](FirebaseChat/AppConstant.h) and set `FIREBASE_URL` to your Firebase url.

## Configure Parse
- Create a new [Parse](https://www.parse.com/).
- Create a new Parse app.
- Edit [`AppConstant.h`](FirebaseChat/AppConstant.h) and set `PARSE_APP_ID` to your Parse app id and `PARSE_CLIENT_KEY` to your Parse app client key.

## Configure Login

- Create a [new Twitter app](https://apps.twitter.com/) to use for login.
- Go to your [Firebase Dashbaord](https://www.firebase.com/account/) and navigate to your Firebase.
- Click `Login & Auth` in the left side menu. Select `Twitter` and check the box to `Enable Twitter Login`
- Paste your Twitter API Key and API Secret into the form.
- Edit [`Constants.swift`](FireChat-Swift/Constants.swift) and set `TWITTER_APP_ID` to your Twitter API Key.
- Run the app on a device or simulator
- On your iOS device or simulator, go to Settings, scroll down to the accounts section (which contains Twitter, Facebook, Flickr and Vimeo), select Twitter -> Add Account.